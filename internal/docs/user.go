package docs

import (
	"gitlab.com/gmalka1/goboilerplate/internal/modules/user/controller"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
//   200: profileResponses

// swagger:parameters profileRequest
type profileRequest struct {
}

// swagger:response profileResponses
type profileResponses struct {
	// in:body
	Body controller.ProfileResponse
}
